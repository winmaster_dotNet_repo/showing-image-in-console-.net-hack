﻿using System;

namespace ShowImgInConsoleHack
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Graphics in console window!");
            FontAproach.FontApproachImgShow();
            Console.ReadKey();
        }
    }
}
